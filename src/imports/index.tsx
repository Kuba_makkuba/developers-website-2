import Box from "@mui/material/Box";
import Bookmarks from "components/bookmarks";
import Form from "components/form";
import Freelancer from "components/freelancer";
import Information from "components/information";
import { Helmet, HelmetProvider } from "react-helmet-async";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { Link } from "react-router-dom";
import Paragraph from "components/paragraph";
import Partners from "components/partners";
import Post from "components/post";
import Profile from "components/profile";
import Projects from "components/projects";
import Skills from "components/skills";
import TextField from "@mui/material/TextField";
import Title from "components/title";
import Tools from "components/tools";

import useMediaQuery from "@mui/material/useMediaQuery";

import easyCode from "assets/easy_code_button_grey.png";
import elementGraficznyTla_01 from "assets/element_graficzny_tła_01.png";
import elementGraficznyTla_02 from "assets/element_graficzny_tła_02.png";
import elementGraficznyTla_03 from "assets/element_graficzny_tła_03_pionowy.png";
import elementGraficznyTla_04 from "assets/element_graficzny_tła_04_pionowy.png";
import elementGraficznyTla_05 from "assets/element_graficzny_tła_05_pionowy.png";
import girl from "assets/girl.png";
import ikonaAboutMe from "assets/ikona_about_me.png";
import portfolioCase_05 from "../assets/portfolio_case_05.png";
import sygnetKwadraty from "assets/sygnet_kwadraty_.png";
import "assets/portfolio_case_01.png";
import "assets/portfolio_case_02.png";
import "assets/portfolio_case_03.png";
import "assets/portfolio_case_04.png";
import "assets/portfolio_case_05.png";
import "assets/portfolio_case_06.png";
import "assets/webpack_icon.png";

import barData from "data/bar.json";
import bookmarksData from "data/bookmarks.json";
import formData from "data/form.json";
import informationData from "data/informations.json";
import navbarData from "data/navbar.json";
import projectsData from "data/projects.json";
import toolsData from "data/tools.json";

export const imports = {
    barData,
    Bookmarks,
    bookmarksData,
    Box,
    easyCode,
    elementGraficznyTla_01,
    elementGraficznyTla_02,
    elementGraficznyTla_03,
    elementGraficznyTla_04,
    elementGraficznyTla_05,
    Form,
    formData,
    Freelancer,
    girl,
    Helmet,
    HelmetProvider,
    ikonaAboutMe,
    Information,
    informationData,
    LazyLoadImage,
    Link,
    navbarData,
    Paragraph,
    Partners,
    portfolioCase_05,
    Post,
    Profile,
    Projects,
    projectsData,
    Skills,
    sygnetKwadraty,
    TextField,
    Title,
    Tools,
    toolsData,
    useMediaQuery
}