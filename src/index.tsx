import { HelmetProvider } from "react-helmet-async";
import ReactDOM from "react-dom";
import { RouterProvider } from "react-router-dom";
import { Suspense } from "react";
import Spinner from "components/spinner";
import Head from "components/head";
import routes from "routes";

import "./styles/themes/default/theme.scss";
import "react-lazy-load-image-component/src/effects/blur.css";

ReactDOM.render(
  <Suspense fallback={<Spinner />}>
    <HelmetProvider>
      <Head/>
      <RouterProvider router={routes} />
    </HelmetProvider>
  </Suspense>,
  document.getElementById("root")
);