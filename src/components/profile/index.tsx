import { imports } from "imports";
import { ProfileType } from "./types";

const Profile = ({ classHeight, classElement, classImage }: ProfileType) => {

  const { girl, elementGraficznyTla_02 } = imports;

  return (
    <div className={classHeight}>
      <img className={classElement} src={elementGraficznyTla_02} alt="elementGraficznyTla_02"/>
      <img className={classImage} src={girl} alt="me" />
    </div>
  );
};

export default Profile;
