export interface ProfileType {
    classHeight: string;
    classElement: string;
    classImage: string;
}