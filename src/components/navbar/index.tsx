import { imports } from "imports";
import { NavbarType } from "./types";

const Navbar = () => {

  const { navbarData, Link, sygnetKwadraty } = imports;
  const { navbar } = navbarData;

  const navigationMap = navbar.map(({ link, icon }: NavbarType, index: number) => (
    <li className="navbar__item" key={index}>
      <Link to={link}>
        <img className="navbar__icon" src={icon} alt="elementGraficzny" />
      </Link>
    </li>
  ));

  return (
    <div className="navbar">
      <ul className="navbar__list">
        <li className="navbar__item">
          <Link to="/">
            <img className="navbar__image" src={sygnetKwadraty} alt="sygnetKwadraty"/>
          </Link>
        </li>
        {navigationMap}
        <span className="navbar__line" />
        {navigationMap}
      </ul>
    </div>
  );
};

export default Navbar;
