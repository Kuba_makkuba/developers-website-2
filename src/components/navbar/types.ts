export interface NavbarType {
    link: string;
    icon: string;
}