import { imports } from "imports";

const Information = () => {

  const { informationData, Paragraph } = imports;

  return informationData.map((item: string, index: number) => (
    <div className="information" key={index}>
      <Paragraph standard={true} text={item} />
    </div>
  ));
};

export default Information;
