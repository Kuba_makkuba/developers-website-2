import { imports } from "imports";

const Form = () => {

  const { Box, TextField, useMediaQuery, formData } = imports;

  const matches = useMediaQuery("(min-width:600px)");
  const fontResponive = matches ? 18 : 12;

  return (
    <Box component="form">
      {formData.map((item: string, index: number) => (
        <TextField
          key={index}
          inputProps={{ style: { fontSize: fontResponive } }}
          InputLabelProps={{ style: { fontSize: fontResponive } }}
          label={item}
          type="search"
          variant="standard"
          fullWidth
          margin="dense"
        />
      ))}
    </Box>
  );
};

export default Form;
