import { imports } from "imports";
import { PostType } from "./types";

const Post = ({ text }: PostType) => {

  const { Title, LazyLoadImage, Paragraph, portfolioCase_05 } = imports;

  return (
    <div className="post">
      <div className="post__boxLoader">
        <LazyLoadImage className="post__image" src={portfolioCase_05} alt="post" effect="blur"/>
      </div>
      <div className="post__wrapper">
        <article>
          <Title title={text} span="Secondary title" img={false} tag="h2"/>
          <Paragraph standard={true} text="author, 12/09/2020" />
          <div className="post__container">
            <Paragraph
              standard={true}
              text="Lorem ipsum, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            />
          </div>
        </article>
        <div className="post__box">
          <Paragraph standard={true} text="Read me"/>
        </div>
      </div>
    </div>
  );
};

export default Post;
