export interface ParagraphType {
    text: string;
    standard: boolean;
}