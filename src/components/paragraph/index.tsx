import { ParagraphType } from "./types"

const Paragraph = ({ text, standard }: ParagraphType) => {

    const isStandard = standard ? "paragraph__text" : "paragraph__name"

    return <p className={isStandard}>{text}</p>
}

export default Paragraph