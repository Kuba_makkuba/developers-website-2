import { imports } from "imports";

const Partners = () => {

  const { Title, Paragraph, elementGraficznyTla_04 } = imports;

  return (
    <div className="partners">
      <img className="partners__image" src={elementGraficznyTla_04} alt="elementGraficznyTla_04"/>
      <Title title="Partners" img={true} tag="h2"/>
      <Paragraph text="Easy Coder" standard={true}/>
      <Paragraph text="TECHOOO Techy Vlog 01" standard={true}/>
    </div>
  );
};

export default Partners;
