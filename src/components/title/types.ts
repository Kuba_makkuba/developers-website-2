export interface TitleType {
    title: string;
    img: boolean;
    span?: string;
    tag: string;
}