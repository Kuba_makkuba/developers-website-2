import { imports } from "imports";
import { TitleType } from "./types";

const Title = ({ title, img, span, tag }: TitleType) => {

  const { sygnetKwadraty } = imports;

  const isImage = img ? <img className="title__image" src={sygnetKwadraty} alt="sygnet"/> : null
  const isSpan = !span ? <span className="title__name">{span}</span> : null
  const tagName = tag === "h1" ? <h1 className="title__text">{title}{isSpan}</h1> : <h2 className="title__text">{title}{isSpan}</h2>

  return(
      <div className="title">
          {isImage}
          {tagName}
      </div>
    )
}

export default Title;