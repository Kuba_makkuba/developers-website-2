import { imports } from "imports";
import { ToolsType } from "./types";

const Tools = () => {

  const { Title, Paragraph, elementGraficznyTla_05, toolsData } = imports;

  const tools = toolsData[0].tools;

  return (
    <div className="tools">
      <img className="tools__picture" src={elementGraficznyTla_05} alt="elementGraficznyTla_05"/>
      <div className="tools__description">
        <Title title="Tools" span="Programms I use" img={true} tag="h2" />
        <Paragraph standard={true} text="Essentials to make the work done." />
      </div>
      <div className="tools__wrapper">
        {tools.map(({ text, image }: ToolsType, index: number) => (
          <div key={index} className="tools__technology">
            <div className="tools__boxImage">
              <img className="tools__image" src={image} alt="tools" />
            </div>
            <p className="tools__name">{text}</p>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Tools;
