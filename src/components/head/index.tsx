import { imports } from "imports";

const Head = () => {

  const { Helmet } = imports;

  return (
      <Helmet>
        <link href="https://fonts.googleapis.com/css2?family=Outfit:wght@300&display=swap" rel="stylesheet"/>
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400&display=swap" rel="stylesheet"/>
        <link rel="icon" type="image/png" href="favicon.ico"/>
        <meta name="description" content="Card"/>
        <meta name="theme-color" content="#98e6df"/>
        <meta name="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="referrer" content="strict-origin"/>
        <meta name="author" content="Jakub Makowski"/>
        <meta name="keywords" content="Card, project, frontend"/>
        <meta name="robots" content="index, follow"/>
        <meta charSet="UTF-8" http-equiv="X-UA-Compatible" name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover"/>
        <meta property="og:title" content="Card"/>
        <meta property="og:description" content="Card"/>
        <title>Card</title>
      </Helmet>
  );
};

export default Head;
