import { BookmarksType } from "./types";
import { imports } from "imports";

const Bookmarks = ({ bookmarks }: BookmarksType) => {

  const { Paragraph, Title } = imports;

  return (
    <div className="bookmarks">
      <Title title="Read" tag="h2" img={false} span={undefined}/>
        {bookmarks.map((item: string, index: number) => (
          <div key={index} className="bookmarks__wrapper">
            <Paragraph text={item} standard={false}/>
            <Paragraph text="&gt;" standard={false}/>
          </div>
        ))}
    </div>
  );
};

export default Bookmarks;
