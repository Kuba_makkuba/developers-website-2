export interface SkillsType {
    percent: string;
    className: string;
    name: string;
}