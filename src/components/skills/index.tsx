import { imports } from "imports";
import { SkillsType } from "./types";

const Skills = () => {

  const { Paragraph, Title, barData, elementGraficznyTla_02 } = imports;

  const bar = barData[0].bar;

  return (
    <div className="skills">
      <img className="skills__graphicPicture" src={elementGraficznyTla_02} alt="elementGraficznyTla_01"/>
      <Title title="My skills" span="Secondary title" img={true} tag="h2" />
      <Paragraph
        standard={true}
        text="Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. "
      />
      <div className="skills__wrapper">
        {bar.map(({ percent, className, name }: SkillsType, index: number) => (
          <div key={index} className="skills__bar">
            <div className="skills__percent">
              <p className="skills__text">{percent}</p>
              <span className={`skills__line ${className}`} />
            </div>
            <div className="skills__technology">
              <p className="skills__text">{name}</p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Skills;
