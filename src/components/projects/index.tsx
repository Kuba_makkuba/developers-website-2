import { imports } from "imports";

const Projects = () => {

  const { LazyLoadImage, elementGraficznyTla_01, elementGraficznyTla_04, ikonaAboutMe, projectsData } = imports;

  return (
    <div className="projects">
      <img className="projects__graphic" src={elementGraficznyTla_04} alt="elementGraficzny"/>
      <img className="projects__picture" src={elementGraficznyTla_01} alt="elementGraficzny"/>
      <div className="projects__wrapper">
        {projectsData.map((item: string, index: number) => (
          <div className="projects__square" key={index}>
            <div className="projects__container">
              <div className="projects__box">
                <div className="projects__gradient">
                  <p className="projects__title">Office Software</p>
                  <p className="projects__name">-task managment</p>
                  <p className="projects__title projects__title--margin">
                    Tools React / Redux /
                  </p>
                  <p className="projects__title projects__title--color">
                    Program 01 / Flexbox
                  </p>
                </div>
                <LazyLoadImage className="projects__image" src={item} alt="portfolio" effect="blur"/>
                <div className="projects__figure">
                  <img className="projects__icon" src={ikonaAboutMe} alt="bitbucket"/>
                  <img className="projects__icon" src={ikonaAboutMe} alt="external_link"/>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Projects;
