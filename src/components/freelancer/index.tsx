import { imports } from "imports";

const Freelancer = () => {

  const { Profile, Title, Paragraph, elementGraficznyTla_03, elementGraficznyTla_04 } = imports;

  return (
    <div className="freelancer">
      <img className="freelancer__picture" src={elementGraficznyTla_03} alt="elementGraficznyTla_03"/>
      <Profile classHeight="profileTwo" classElement="profile__graphicPictureOne" classImage="profile__imageOne"/>
      <Title title="I am a freelancer" img={true} tag="h2"/>
      <Paragraph
        standard={true}
        text="You can ask me to join the conference or hire me to work with you on some project. Please, send me a short brief or contact me via e-mail. I will be happy to help you."
      />
      <div className="freelancer__wrapper">
        <img className="freelancer__image" src={elementGraficznyTla_04} alt="elementGraficznyTla_04"/>
        <button className="freelancer__button">Download CV</button>
        <button className="freelancer__button">Hire me</button>
      </div>
    </div>
  );
};

export default Freelancer;
