const Spinner = () => {
  return (
    <div className="lds">
      <div className="lds__ring" />
    </div>
  );
};

export default Spinner;
