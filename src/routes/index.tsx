import React from "react";
import { createBrowserRouter } from "react-router-dom";

import AboutMe from "pages/aboutMe";
import Blog from "pages/blog";
import Contact from "pages/contact";
import Navbar from "components/navbar";
import Portfolio from "pages/portfolio";
import Passionate from "pages/passionate";

const routesData = [
  { path: "/", component: Passionate },
  { path: "/portfolio", component: Portfolio },
  { path: "/contact", component: Contact },
  { path: "/blog", component: Blog },
  { path: "/about", component: AboutMe }
];

const routes = createBrowserRouter(
  routesData.map(({ path, component }) => ({
    path,
    element: (
      <div className="main">
        <Navbar />
        {React.createElement(component)}
      </div>
    )
  }))
);

export default routes;
