import { imports } from "imports";

const Blog = () => {

  const { Title, Paragraph, Post, Profile, Bookmarks, bookmarksData, elementGraficznyTla_01, elementGraficznyTla_03 } = imports;

  return (
    <div className="blog">
      <div className="blog__main">
        <div className="blog__wrapperOne">
          <img className="blog__image" src={elementGraficznyTla_01} alt="elementGraficznyTla_01"/>
          <Title title="Blog" span="My subjective point of programming" img={true} tag="h1"/>
          <Paragraph
            standard={true}
            text="Lorem ipsum, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
          />
          <Post text="Blog post 01" />
          <Post text="Blog post 02" />
        </div>
        <div className="blog__wrapperTwo">
          <img className="blog__picture" src={elementGraficznyTla_03} alt="elementGraficznyTla_03"/>
          <Profile classElement="profile__graphicPictureOne" classHeight="profileOne" classImage="profile__imageTwo"/>
          <Bookmarks bookmarks={bookmarksData[0]?.categories} />
          <div className="blog__information">
            <h3>Read about</h3>
            <Paragraph
              standard={false}
              text="#freelance&nbsp;&nbsp;&nbsp; #online courses&nbsp;&nbsp;&nbsp; #networking&nbsp;&nbsp;&nbsp; #home office&nbsp;&nbsp;&nbsp; #programming&nbsp;&nbsp;&nbsp; #blog"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Blog;
