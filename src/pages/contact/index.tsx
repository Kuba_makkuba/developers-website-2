import { imports } from "imports";

const Contact = () => {

  const { Title, Paragraph, Form, Profile, Bookmarks, Information, Partners, bookmarksData, elementGraficznyTla_01 } = imports;

  return (
    <div className="contact">
      <div className="contact__main">
        <div className="contact__wrapperOne">
          <div className="contact__form">
            <Title title="Contact me" img={true} tag="h1" />
            <Paragraph
              standard={true}
              text="If you are willing to work with me, contact me. I can join your conference to serve you with my engeneering experience."
            />
            <Form />
          </div>
        </div>
        <div className="contact__wrapperTwo">
          <div className="contact__profile">
            <Profile classElement="profile__graphicPictureTwo" classHeight="profileOne" classImage="profile__imageTwo"/>
            <Bookmarks bookmarks={bookmarksData[0]?.read} />
          </div>
        </div>
      </div>
      <div className="contact__box">
        <img className="contact__image" src={elementGraficznyTla_01} alt="elementGraficznyTla_01"/>
        <div className="contact__information">
          <Information />
        </div>
        <Partners />
      </div>
    </div>
  );
};

export default Contact;
