import { imports } from "imports";

const Passionate = () => {

  const { Title, Paragraph, Information, Freelancer, ikonaAboutMe, elementGraficznyTla_01, easyCode } = imports;

  return (
    <div className="passionate">
      <div className="passionate__wrapper">
        <div className="passionate__main">
          <div className="passionate__container">
            <Title title="Hi! My name is Jane Doe" span="Passionate software engineer" img={true} tag="h1"/>
            <div className="passionate__box">
              <Paragraph
                standard={true}
                text="Passionate Techy and Tech Author with 5 years of valuable experience within the field of software engineering."
              />
            </div>
            <Paragraph standard={true} text="See my works on Bitbucket and DEV"/>
            <div className="passionate__aboutMe">
              <img className="passionate__image" src={ikonaAboutMe} alt="ikonaAboutMe"/>
              <img className="passionate__image" src={ikonaAboutMe} alt="ikonaAboutMe"/>
            </div>
            <div className="passionate__figure">
              <img className="passionate__picture" src={elementGraficznyTla_01} alt="elementGraficznyTla_01"/>
            </div>
          </div>
          <div className="passionate__information">
            <Information />
            <div className="passionate__frame">
              <Paragraph standard={true} text="Ukończyłem kurs Easy Code" />
              <img className="passionate__icon" src={easyCode} alt="easyCode" />
            </div>
          </div>
        </div>
        <Freelancer />
      </div>
    </div>
  );
};

export default Passionate;
