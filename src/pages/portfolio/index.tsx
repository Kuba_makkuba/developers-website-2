import { imports } from "imports";

const Portfolio = () => {

  const { Title, Paragraph, Projects, Profile } = imports;

  return (
    <div className="portfolio">
      <div className="portfolio__wrapper">
        <div className="portfolio__container">
          <Title title="Portfolio" span="See the works I enjoyed working on" img={true} tag="h1"/>
          <Paragraph
            standard={true}
            text="Lorem ipsum, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
          />
          <div className="portfolio__box">
            <h3 className="portfolio__text">CATEGORY 01</h3>
            <h3 className="portfolio__text">CATEGORY 02</h3>
            <h3 className="portfolio__text">CATEGORY 03</h3>
            <h3 className="portfolio__text">CATEGORY 04</h3>
          </div>
        </div>
        <Profile
          classElement="profile__graphicPictureOne"
          classHeight="profileOne"
          classImage="profile__imageTwo"
        />
      </div>
      <Projects />
    </div>
  );
};

export default Portfolio;
