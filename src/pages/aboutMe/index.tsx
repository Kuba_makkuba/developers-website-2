import { imports } from "imports";

const AboutMe = () => {

  const { Title, Skills, Tools, Paragraph, elementGraficznyTla_03, elementGraficznyTla_01, ikonaAboutMe } = imports;

  return (
    <div className="aboutMe">
      <div className="aboutMe__main">
        <img className="aboutMe__image" src={elementGraficznyTla_03} alt="elementGraficznyTla_03"/>
        <div className="aboutMe__container">
          <div className="aboutMe__wrapper">
            <img className="aboutMe__picture" src={elementGraficznyTla_01} alt="elementGraficznyTla_01"/>
            <Title title="About me" span="All about Techy" img={true} tag="h1"/>
            <Paragraph text="Lorem ipsum dolor sit amet, " standard={true} />
            <Paragraph
              standard={true}
              text="consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            />
            <div className="aboutMe__bitbucket">
              <Paragraph text="See my works on Bitbucket and DEV" standard={true}/>
              <div className="aboutMe__box">
                <img className="aboutMe__icon" src={ikonaAboutMe} alt="ikonaAboutMe"/>
                <img className="aboutMe__icon" src={ikonaAboutMe} alt="ikonaAboutMe"/>
              </div>
            </div>
          </div>
          <div className="aboutMe__interests">
            <h3>My interests</h3>
            <ul>
              <li className="aboutMe__item">music</li>
              <li className="aboutMe__item">jogging</li>
              <li className="aboutMe__item">mountain climbing</li>
              <li className="aboutMe__item">art</li>
            </ul>
          </div>
        </div>
        <div className="aboutMe__container">
          <Skills />
        </div>
      </div>
      <Tools />
    </div>
  );
};

export default AboutMe;
